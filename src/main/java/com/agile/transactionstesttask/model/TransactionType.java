package com.agile.transactionstesttask.model;

public enum TransactionType {

    CREDIT, DEBIT;
}
