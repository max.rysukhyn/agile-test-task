package com.agile.transactionstesttask.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class Transaction {

    Long id;
    String type;
    BigDecimal amount;
    LocalDateTime effectiveDate;
}
