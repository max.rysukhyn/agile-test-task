package com.agile.transactionstesttask.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransactionRequest {

    TransactionType transactionType;
    BigDecimal amount;
}
