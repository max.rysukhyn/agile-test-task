package com.agile.transactionstesttask.service;

import com.agile.transactionstesttask.model.Transaction;
import com.agile.transactionstesttask.model.TransactionRequest;
import com.agile.transactionstesttask.model.TransactionType;
import com.agile.transactionstesttask.repository.TransactionRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static com.agile.transactionstesttask.databaseemulation.DataBase.ACCOUNT;

@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class TransactionService {

    final TransactionRepository repository;

    public Transaction get(Long id) {
        log.debug("Get transaction. id = {}", id);

        return repository.findById(id).orElseThrow(() -> new RuntimeException("Not found transaction"));
    }

    public boolean makeTransaction(TransactionRequest request) {
        log.debug("make new transaction");

        return TransactionType.CREDIT.equals(request.getTransactionType()) ?
                makeCreditTransaction(request.getAmount()) :
                makeDebitTransaction(request.getAmount());
    }

    public List<Transaction> getAll() {
        log.debug("Find all transactions");

        return repository.findAll();
    }


    private synchronized boolean makeCreditTransaction(BigDecimal amount) {
        ACCOUNT.setAmount(ACCOUNT.getAmount().add(amount));

        repository.put(
                new Transaction()
                        .setAmount(amount)
                        .setEffectiveDate(LocalDateTime.now())
                        .setType(TransactionType.CREDIT.toString())
        );

        return true;
    }

    private synchronized boolean makeDebitTransaction(BigDecimal amount) {
        BigDecimal resultAmount = ACCOUNT.getAmount().subtract(amount);

        if (resultAmount.compareTo(BigDecimal.ZERO) < 0) {
            log.error("Insufficient funds for debit transaction. Amount: {}", amount);
            return false;
        }

        ACCOUNT.setAmount(resultAmount);

        repository.put(
                new Transaction()
                        .setAmount(amount)
                        .setEffectiveDate(LocalDateTime.now())
                        .setType(TransactionType.DEBIT.toString())
        );

        return true;
    }
}
