package com.agile.transactionstesttask.service;

import com.agile.transactionstesttask.repository.BalanceRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BalanceService {

    final BalanceRepository repository;

    public BigDecimal get() {
        return repository.get();
    }

}
