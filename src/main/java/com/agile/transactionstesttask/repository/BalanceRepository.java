package com.agile.transactionstesttask.repository;

import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

import static com.agile.transactionstesttask.databaseemulation.DataBase.ACCOUNT;

@Repository
public class BalanceRepository {

    public BigDecimal get() {
        return ACCOUNT.getAmount();
    }
}
