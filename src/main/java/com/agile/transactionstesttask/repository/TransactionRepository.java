package com.agile.transactionstesttask.repository;

import com.agile.transactionstesttask.model.Transaction;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.agile.transactionstesttask.databaseemulation.DataBase.TRANSACTIONS;
import static com.agile.transactionstesttask.databaseemulation.DataBase.TRANSACTION_ID;

@Repository
public class TransactionRepository {

    public Optional<Transaction> findById(Long id) {
        return Optional.ofNullable(TRANSACTIONS.get(id));
    }

    public void put(Transaction transaction) {
        TRANSACTIONS.put(TRANSACTION_ID.incrementAndGet(), transaction.setId(TRANSACTION_ID.get()));
    }

    public List<Transaction> findAll() {
        return new ArrayList<>(TRANSACTIONS.values());
    }
}
