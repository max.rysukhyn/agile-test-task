package com.agile.transactionstesttask.databaseemulation;

import com.agile.transactionstesttask.model.Account;
import com.agile.transactionstesttask.model.Transaction;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class DataBase {

    public static final Map<Long, Transaction> TRANSACTIONS = new HashMap<>();

    public static AtomicLong TRANSACTION_ID = new AtomicLong(0);

    public static final Account ACCOUNT = new Account(BigDecimal.ZERO);
}
