package com.agile.transactionstesttask.controller;


import com.agile.transactionstesttask.service.BalanceService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping("/balance")
@RequiredArgsConstructor
public class BalanceController {

    BalanceService service;

    @GetMapping
    public ResponseEntity<BigDecimal> get() {
        return ResponseEntity.ok(service.get());
    }
}
